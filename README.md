# Notes from reading Bayesian Data Analysis

Third edition by Andrew Gelman, John B. Carlin, Hal S. Ster,
David B. Dunson, Aki Vehtari, and Donald B. Rubin.  CRC Press,
Texts in Statistical Science.